//
//  AppDelegate.h
//  Hello World
//
//  Created by Amanda Kobiolka on 1/22/17.
//  Copyright © 2017 Project1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

