//
//  ViewController.h
//  Hello World
//
//  Created by Amanda Kobiolka on 1/22/17.
//  Copyright © 2017 Project1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
- (IBAction)button:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *imageview;

@end

