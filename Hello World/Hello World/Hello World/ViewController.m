//
//  ViewController.m
//  Hello World
//
//  Created by Amanda Kobiolka on 1/22/17.
//  Copyright © 2017 Project1. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)button:(id)sender {
    NSArray *myArray = @[@"Hello World!", @"¡Hola Mundo!", @"Ciao mondo!", @"Hallo Welt!", @"Dia duit an Domhain!", @"Witaj świecie!", @"Bonjour le monde!"];
    NSArray *testArray = @[@"usa.jpeg", @"spain.jpeg", @"italy.jpeg", @"germany.jpeg",
                           @"ireland.jpeg", @"poland.jpeg", @"france.jpeg"];
    int index = arc4random_uniform(7);
    _imageview.image = [UIImage imageNamed:testArray[index]];
    _label.text = myArray[index];

}
@end
